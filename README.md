# Julia Reduce

The function `binarytree_reduce()` implements a reduce in Julia. A reduce operation take a commutative binary operator and an array of object, and gives the result of applying recursively the operator to pairs of objects.

The function `reduce_sum()` gives an example where all the parts of a `DArray` (see `DistributedArrays.jl` library) are sumed.

Both use the `Distributed.jl` library for communication.
